#!/usr/bin/env bash

set -e

# Colors
green='\033[0;32m'
yellow='\033[0;93m'
no_color='\033[0m'
cyan='\033[0;36m'
red='\033[0;31m'

# Functions
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

get_email_from_input() {
    trimmed_email=
    email_regex="^[a-z0-9!#\$%&'*+/=?^_\`{|}~-]+(\.[a-z0-9!#$%&'*+/=?^_\`{|}~-]+)*@([a-z0-9]([a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]([a-z0-9-]*[a-z0-9])?\$"
    while ! [[ "${trimmed_email}" =~ ${email_regex} ]]; do
       read -p "$(echo -e "${cyan}Please enter your ${git_host} email: ${no_color}")" email
       trimmed_email=$(echo -n "${email//[[:space:]]/}")
    done
    echo "${trimmed_email}"
}

get_password_from_input() {
    password=""
    while [[ ${password} = "" ]]; do
       read -s -p "$(echo -e "${cyan}Please enter your ${git_host} password: ${no_color}")" password
    done
    echo "${password}"
}

generate_ssh_key() {
    email=$1
    rm -f ~/.ssh/id_rsa
    rm -f ~/.ssh/id_rsa.pub
    ssh-keygen -t rsa -b 4096 -C "${email}" -f ~/.ssh/id_rsa -N ''
    if [[ -z $(echo "${SSH_AGENT_PID}") ]] || [[ ! $(ps -p "${SSH_AGENT_PID}" 1>/dev/null 2>/dev/null) ]]; then
        eval `ssh-agent -s`
    fi
    ssh-add ~/.ssh/id_rsa
}

git_ssh_connected() {
    echo "Checking git connection to ${git_host}"
    if ssh "git@${git_host}" -o ConnectTimeout=5 1>/dev/null 2>/dev/null; [[ $? -ne 255 ]]; then
        echo "Connected to ${git_host}";
        git_connect_succeeded="true"
        return 0
    else
        echo "SSH connection failed to connect to ${git_host}";
        git_connect_succeeded="false"
        return 1
    fi;
}

# Variables
SCRIPT_PATH=$(realpath $0)
SCRIPT_DIRECTORY=$(dirname ${SCRIPT_PATH})
PROJECT_DIRECTORY=$(dirname ${SCRIPT_DIRECTORY})

echo ""
echo -e "${green}configuring ssh key${no_color}"

answer=
while [[ ${answer} = "" ]] || ( [[ "${answer}" != "y" ]] && [[ "${answer}" != "n" ]] ); do
   read -p "$(echo -e "${cyan}Would you like to configure ssh? (y/n): ${no_color}")" answer
   if [[ "${answer}" != "y" ]] && [[ "${answer}" != "n" ]]; then
      echo -e "${red}Please enter 'y' or 'n'${no_color}"
   fi
done
echo ""
if [[ "${answer}" = "n" ]]; then
  echo "Skipping ssh configuration"
  exit 0
fi

# Add git host to known hosts
answer=
while [[ ${answer} = "" ]]; do
   read -p "$(echo -e "${cyan}Please enter your gitlab host (example: gitlab.com): ${no_color}")" answer
done
git_host=${answer}
mkdir -p ~/.ssh/
touch ~/.ssh/known_hosts
if [[ -z $(grep "${git_host}" ~/.ssh/known_hosts) ]]; then
    echo "Adding "${git_host}" to known hosts"
    ssh-keyscan "${git_host}" >> ~/.ssh/known_hosts
else
    echo "${git_host} is already in known hosts"
fi

# Test git authentication
set +e
git_ssh_connected
set -e

# Name
touch ~/.gitconfig
trimmed_name=$1
if [[ -z "${trimmed_name}" ]] && [[ -z "$(git config --global -l | grep "user.name")" ]]; then
    trimmed_name=
    while [[ "${trimmed_name}" = "" ]]; do
       read -p "Please enter your full name: " name
       trimmed_name=$(echo -n "${name//[[:space:]]/}")
    done
fi
if [[ -z "$(git config --global -l | grep "user.name")" ]]; then
    git config --global "user.name" ${trimmed_name}
fi

# Email
email=$2
if [[ -z "${email}" ]]; then
    if [[ -z "$(git config --global -l | grep "user.email")" ]] || [[ ${git_connect_succeeded} != "true" ]]; then
        email=$(get_email_from_input)
    fi
fi
if [[ -z "$(git config --global -l | grep "user.email")" ]]; then
   git config --global "user.email" ${email}
fi

# SSH Key
if [[ ${git_connect_succeeded} != "true" ]] && [[ ! -f ~/.ssh/id_rsa ]]; then
    generate_ssh_key "${email}"
fi

# Password
password=$3
if [[ ${git_connect_succeeded} != "true" ]]; then

    if [[ -z "${password}" ]]; then
        password=$(get_password_from_input)
        echo ""
    fi

    while [[ ${git_connect_succeeded} != "true" ]]; do

        echo "Uploading public key to ${git_host}"
        ${PROJECT_DIRECTORY}/gitlab/upload-public-key.sh "${git_host}" "${email}" "${password}"

        set +e
        if ! git_ssh_connected ; then
            set -e
            echo -e "Login failed. Please re-enter your email and password"

            email=$(get_email_from_input)
            echo ""
            password=$(get_password_from_input)
            echo ""

            generate_ssh_key "${email}"
        fi
        set -e
    done

    git config --global "user.email" ${email}
fi