#!/usr/bin/env bash

set -e

git config --global pull.default current
git config --global push.default current