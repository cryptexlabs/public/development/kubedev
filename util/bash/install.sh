#!/usr/bin/env bash

set -e

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Colors
green='\033[0;32m'
yellow='\033[0;93m'
red='\033[0;31m'
no_color='\033[0m'

# Inputs
if [[ -z "${1}" ]]; then
    echo -e "${red}Missing argument 1 for operating system${no_color}"
fi

operating_system=$1
disable_sudo=$2

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))
PROJECT_DIRECTORY=$(dirname $(dirname ${SCRIPT_DIRECTORY}))
OS_DIRECTORY="${PROJECT_DIRECTORY}/${operating_system}"

# Enable passwordless sudo
${OS_DIRECTORY}/sudo/make-current-user-sudoer.sh
${OS_DIRECTORY}/sudo/enable-passwordless-sudo.sh

## SSH
name=
email=
password=
source ${OS_DIRECTORY}/local-tools/get-user-info.sh
${OS_DIRECTORY}/local-tools/pre-ssh-install.sh
${PROJECT_DIRECTORY}/git/configure-ssh.sh ${name} ${email} ${password}
${PROJECT_DIRECTORY}/git/configure-options.sh

# Local tools
${OS_DIRECTORY}/local-tools/install.sh

# Disable passwordless sudo
if [[ ${disable_sudo} != "false" ]]; then
    ${OS_DIRECTORY}/sudo/disable-passwordless-sudo.sh
fi

echo ""
echo "**************************************************************************************"
echo ""
echo -e "${green}Success! kubedev installation is complete. Happy Coding!${no_color}"
echo ""
echo "**************************************************************************************"
echo ""

sleep 5
