#!/usr/bin/env bash

set -e

cyan='\033[0;36m'
no_color='\033[0m'
did_prompt_user="false"

OS_DIRECTORY=$1
source ${OS_DIRECTORY}/fn.sh

get_email_from_input() {
    trimmed_email=
    email_regex="^[a-z0-9!#\$%&'*+/=?^_\`{|}~-]+(\.[a-z0-9!#$%&'*+/=?^_\`{|}~-]+)*@([a-z0-9]([a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]([a-z0-9-]*[a-z0-9])?\$"
    while ! [[ "${trimmed_email}" =~ ${email_regex} ]]; do
       read -p "$(echo -e "${cyan}Please enter your ${git_host} email: ${no_color}")" email
       trimmed_email=$(echo -n "${email//[[:space:]]/}")
    done
    did_prompt_user="true"
    echo "${trimmed_email}"
}

get_password_from_input() {
    password=""
    while [[ ${password} = "" ]]; do
       read -s -p "$(echo -e "${cyan}Please enter your ${git_host} password: ${no_color}")" password
    done
    did_prompt_user="true"
    echo "${password}"
}

get_name_from_input() {
    trimmed_name=""
    while [[ "${trimmed_name}" = "" ]]; do
       read -p "$(echo -e "${cyan}Please enter your full name: ${no_color}")" name
       trimmed_name=$(echo -n "${name//[[:space:]]/}")
    done
    did_prompt_user="true"
    echo "${trimmed_name}"
}

git_ssh_connected() {
    echo "Checking git connection to ${git_host}"
    if ssh "git@${git_host}" -o ConnectTimeout=5 1>/dev/null 2>/dev/null; [[ $? -ne 255 ]]; then
        echo "Connected to ${git_host}";
        return 0
    else
        echo "SSH connection failed to connect to ${git_host}";
        return 1
    fi;
}

green='\033[0;32m'
no_color='\033[0m'

git_host="gitlab.com"

trimmed_name=
email=
password=
if $(git_is_installed) && [[ -f ~/.ssh/known_hosts ]]; then
    if  [[ -z "$(git config --global -l | grep "user.name")" ]]; then
        trimmed_name=$(get_name_from_input)
        git config --global "user.name" ${trimmed_name}
    fi

    if ! git_ssh_connected ; then
        email=$(get_email_from_input)
        password=$(get_password_from_input)
    fi
else
    trimmed_name=$(get_name_from_input)
    email=$(get_email_from_input)
    password=$(get_password_from_input)
fi

export name=${trimmed_name}
export email=${email}
export password=${password}
export did_prompt_user=${did_prompt_user}