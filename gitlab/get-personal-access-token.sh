#!/usr/bin/env bash

set -e

# Colors
red='\033[0;31m'
no_color='\033[0m'

# Inputs
gitlab_host=$1
gitlab_email=$2
gitlab_password=$3

if [[ -z "${gitlab_host}" ]]; then
    echo -e "${red}Missing argument 1 for gitlab host${no_color}"
    exit 1
fi

if [[ -z "${gitlab_email}" ]]; then
    echo -e "${red}Missing argument 2 for gitlab email${no_color}"
    exit 1
fi

if [[ -z "${gitlab_password}" ]]; then
    echo -e "${red}Missing argument 3 for gitlab password${no_color}"
    exit 1
fi

# curl for the login page to get a session cookie and the sources with the auth tokens
body_header=$(curl -L -s \
-c /tmp/gitlab-cookies.txt \
-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' \
-H 'Accept-Encoding: gzip;q=0, deflate, br' \
-H 'Accept-Language: en-US,en;q=0.9,es;q=0.8' \
-H 'Connection: keep-alive' \
-H "Host: ${gitlab_host}" \
-H 'Upgrade-Insecure-Requests: 1' \
-H "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36" \
-i "https://${gitlab_host}/users/sign_in" \
-s)

# grep the authenticity token for the user login for
csrf_token=$(echo ${body_header} | perl -ne 'print "$1\n" if /new_user.*?authenticity_token"[[:blank:]]value="(.+?)"/' | sed -n 1p)

# grep the gitlab session
gitlab_session=$(cat /tmp/gitlab-cookies.txt | perl -ne 'print "$1\n" if /[[:blank:]]_gitlab_session[[:blank:]](.+$)/' | sed -n 1p)

# send login credentials with curl, using cookies and token from previous request to get logged in cookie
curl -L -s \
-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' \
-H 'Accept-Encoding: gzip;q=0, deflate, br' \
-H 'Accept-Language: en-US,en;q=0.9,es;q=0.8' \
-H 'Cache-Control: max-age=0' \
-H 'Connection: keep-alive' \
-H 'Content-Type: application/x-www-form-urlencoded' \
-H "Cookie: _gitlab_session=${gitlab_session}" \
-H "Host: ${gitlab_host}" \
-H "Origin: https://${gitlab_host}" \
-H "Referer: https://${gitlab_host}/users/sign_in" \
-H 'Upgrade-Insecure-Requests: 1' \
-H "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36" \
-b /tmp/gitlab-cookies.txt \
-c /tmp/gitlab-cookies.txt \
-i "https://${gitlab_host}/users/sign_in" \
    --data "user[login]=${gitlab_email}&user[password]=${gitlab_password}" \
    --data-urlencode "authenticity_token=${csrf_token}" \
    -o /dev/null

# send curl GET request to personal access token page to get authenticity token
body_header=$(curl -L -s \
-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' \
-H 'Accept-Encoding: gzip;q=0, deflate, br' \
-H 'Accept-Language: en-US,en;q=0.9,es;q=0.8' \
-H 'Cache-Control: max-age=0' \
-H 'Connection: keep-alive' \
-H "Cookie: _gitlab_session=${gitlab_session}" \
-H "Host: ${gitlab_host}" \
-H 'Upgrade-Insecure-Requests: 1' \
-H "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36" \
-b /tmp/gitlab-cookies.txt \
-c /tmp/gitlab-cookies.txt \
-i "https://${gitlab_host}/profile/personal_access_tokens" \
-s)
csrf_token=$(echo ${body_header} | perl -ne 'print "$1\n" if /authenticity_token"[[:blank:]]value="(.+?)"/' | sed -n 1p)

# curl POST request to send the "generate personal access token form"
# the response will be a redirect, so we have to follow using `-L`
personal_access_token_name="Auto Generated on $(date) by $0"
body_header=$(curl -s -L -b /tmp/gitlab-cookies.txt "https://${gitlab_host}/profile/personal_access_tokens" \
    --data-urlencode "authenticity_token=${csrf_token}" \
    --data "personal_access_token[name]=${personal_access_token_name}&personal_access_token[expires_at]=&personal_access_token[scopes][]=api")

# Scrape the personal access token from the response HTML
personal_access_token=$(echo ${body_header} | perl -ne 'print "$1\n" if /created-personal-access-token"[[:blank:]]value="(.+?)"/' | sed -n 1p)

echo "${personal_access_token}"