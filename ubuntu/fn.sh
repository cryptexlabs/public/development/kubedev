#!/usr/bin/env bash

git_is_installed() {
    if [[ $(which git) ]]; then
        return 0
    else
        return 1
    fi
}