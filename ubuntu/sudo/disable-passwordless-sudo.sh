#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
echo -e "${green}Disabling passwordless sudo${no_color}"
sudo mv /etc/sudoers.bk /etc/sudoers