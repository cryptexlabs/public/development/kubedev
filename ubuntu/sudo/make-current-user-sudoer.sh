#!/usr/bin/env bash

set -e

echo -e "${green}Making current user a sudoer${no_color}"

if sudo grep -q $(whoami) "/etc/sudoers"; then
    echo "$(whoami) is already a sudoer"
else
    sudo cat /etc/sudoers > /tmp/sudoers
    sudo echo "$(whoami) ALL=(ALL) ALL" >> /tmp/sudoers
    sudo chown root /tmp/sudoers
    sudo mv /tmp/sudoers /etc/sudoers
fi