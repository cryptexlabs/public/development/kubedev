#!/usr/bin/env bash

set -e

# Colors
green='\033[0;32m'
no_color='\033[0m'

realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

SCRIPT_DIRECTORY=$(dirname $(realpath $0))
PROJECT_DIRECTORY=$(dirname $(dirname ${SCRIPT_DIRECTORY}))

did_prompt_user=
source ${PROJECT_DIRECTORY}/util/bash/get-user-info.sh "${PROJECT_DIRECTORY}/ubuntu"

echo ""
echo ""
echo ""
if [[ "${did_prompt_user}" == "true" ]] ; then
    echo -e "${green}That's all the info we need from you!${no_color}"
fi

echo -e "${green}When the installation is complete you will need to reboot your machine.${no_color}"
echo -e "${green}Feel free to grab a coffee and relax!${no_color}"
echo ""
echo ""

sleep 10