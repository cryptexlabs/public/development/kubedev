#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ ! $(which git) ]]; then
    echo -e "${green}installing git${no_color}"
    sudo apt-get -y install git
else
    echo -e "${green}git already installed. Updating git${no_color}"
    sudo apt-get -y install --only-upgrade git
fi