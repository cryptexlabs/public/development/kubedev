#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

install_authenticator() {
    curl -o aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.13.7/2019-06-11/bin/linux/amd64/aws-iam-authenticator
    curl -o aws-iam-authenticator.sha256 https://amazon-eks.s3-us-west-2.amazonaws.com/1.13.7/2019-06-11/bin/linux/amd64/aws-iam-authenticator.sha256
    openssl sha1 -sha256 aws-iam-authenticator
    chmod +x ./aws-iam-authenticator
    mkdir -p $HOME/bin && cp ./aws-iam-authenticator $HOME/bin/aws-iam-authenticator && export PATH=$HOME/bin:$PATH
}
echo ""
if [[ -z $(brew ls | grep aws-iam-authenticator) ]]; then
    echo -e "${green}installing aws iam authenticator${no_color}"
    brew install aws-iam-authenticator
else
    echo -e "${green}aws iam authenticator is already installed. Updating aws iam authenticator${no_color}"
    set +e
    brew upgrade aws-iam-authenticator 2> /dev/null
    set -e
fi