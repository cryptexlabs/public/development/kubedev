#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ ! $(which gpg) ]]; then
    echo -e "${green}installing gpg${no_color}"
    sudo apt-get -y install gnupg
else
    echo -e "${green}gpg already installed. Updating gpg${no_color}"
    sudo apt-get -y install --only-upgrade gnupg
fi