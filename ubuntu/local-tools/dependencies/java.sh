#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ ! $(which java) ]]; then
    echo -e "${green}installing java${no_color}"
    sudo apt-get -y install openjdk-8-jdk
else
    echo -e "${green}java already installed. Updating java${no_color}"
    sudo apt-get -y install --only-upgrade openjdk-8-jdk
fi