#!/usr/bin/env bash

set -e

green='\033[0;32m'
yellow='\033[0;93m'
no_color='\033[0m'

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))
UBUNTU_DIRECTORY=$(dirname $(dirname ${SCRIPT_DIRECTORY}))
PROJECT_DIRECTORY=$(dirname ${UBUNTU_DIRECTORY})

# Helper functions
source "${PROJECT_DIRECTORY}/util/bash/fn.sh"

helm_latest=$(get_github_latest_release "helm/helm")
install_helm() {
   curl https://raw.githubusercontent.com/helm/helm/master/scripts/get | bash
}

echo ""
if [[ ! $(which helm) ]]; then
    echo -e "${green}installing helm${no_color}"
    install_helm
    helm init
else
    echo -e "${green}helm already installed. Updating helm${no_color}"
    install_helm
fi

if [[ -z $(helm plugin list | grep secrets) ]]; then
    echo "Installing Helm 'secrets' plugin"
    helm plugin install https://github.com/futuresimple/helm-secrets
else
    echo "Helm 'secrets' plugin already installed. Updating plugin"
    helm plugin update secrets
fi

if [[ -z $(helm plugin list | grep delete-all) ]]; then
    echo "Installing helm 'delete-all' plugin"
    helm plugin install https://github.com/astronomerio/helm-delete-all-plugin --version 0.0.2
else
    echo "Helm 'delete-all' plugin already installed. Updating plugin"
    helm plugin update delete-all
fi