#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ ! $(which hosts) ]]; then
    echo -e "${green}installing hosts manager${no_color}"
    curl -L https://raw.github.com/alphabetum/hosts/master/hosts -o /tmp/hosts
    sudo chmod +x /tmp/hosts
    sudo mv /tmp/hosts /usr/local/bin/
else
    echo -e "${green}hosts manager is already installed. Skipping${no_color}"
fi