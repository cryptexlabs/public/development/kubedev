#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

install_brave() {
    curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -
    sudo sh -c 'echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com `lsb_release -sc` main" >> /etc/apt/sources.list.d/brave.list'
    sudo apt update
    sudo apt install brave-browser brave-keyring
}

echo ""
if [[ ! -d /usr/lib/brave-browser ]]; then
    echo -e "${green}installing brave browser${no_color}"
    install_brave
else
    echo -e "${green}brave browser already installed${no_color}"
    sudo apt-get -y install --only-upgrade brave-browser brave-keyring
fi

echo "Adding util for adding self signed certificates"
sudo apt-get install libnss3-tools
mkdir -p $HOME/.pki/nssdb

echo "Making brave your default browser"
sudo update-alternatives --install /usr/bin/x-www-browser x-www-browser /usr/bin/brave-browser 200
sudo update-alternatives --set x-www-browser /usr/bin/brave-browser