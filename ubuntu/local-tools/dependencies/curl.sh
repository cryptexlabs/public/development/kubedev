#!/usr/bin/env bash

set -e

#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ ! $(which curl) ]]; then
    echo -e "${green}installing curl${no_color}"
    sudo apt-get -y install curl
else
    echo -e "${green}curl already installed. Updating curl${no_color}"
    sudo apt-get -y install --only-upgrade curl
fi