#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ ! $(which yarn) ]]; then
    echo -e "${green}installing yarn${no_color}"
    sudo apt-get -y install yarn
else
    echo -e "${green}yarn already installed. Updating yarn${no_color}"
    sudo apt-get -y install --only-upgrade yarn
fi