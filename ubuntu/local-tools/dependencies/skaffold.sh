#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

install_skaffold() {
    curl -L -o ~/Downloads/skaffold https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64
    chmod +x ~/Downloads/skaffold
    sudo mv ~/Downloads/skaffold /usr/local/bin
}

echo ""
if [[ ! $(which skaffold) ]]; then
    echo -e "${green}installing skaffold${no_color}"
    install_skaffold
else
    echo -e "${green}skaffold already installed. Updating skaffold${no_color}"
    install_skaffold
fi