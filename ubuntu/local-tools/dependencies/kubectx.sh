#!/usr/bin/env bash

set -e

# Colors
green='\033[0;32m'
no_color='\033[0m'

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))
UBUNTU_DIRECTORY=$(dirname $(dirname ${SCRIPT_DIRECTORY}))
PROJECT_DIRECTORY=$(dirname ${UBUNTU_DIRECTORY})

# Helper functions
source "${PROJECT_DIRECTORY}/util/bash/fn.sh"

kubectx_version=$(get_github_latest_release "ahmetb/kubectx")
echo ""
if [[ ! $(which kubectx) ]]; then
    echo -e "${green}installing kubectx${no_color}"
    wget -c "https://github.com/ahmetb/kubectx/archive/v${kubectx_version}.tar.gz" -O - | tar -xz -C /tmp
    sudo mv "/tmp/kubectx-${kubectx_version}/kubectx" /usr/local/bin
    sudo mv "/tmp/kubectx-${kubectx_version}/kubens" /usr/local/bin
    rm -r "/tmp/kubectx-${kubectx_version}"
else
    echo -e "${green}kubectx already installed. Skipping${no_color}"
fi

