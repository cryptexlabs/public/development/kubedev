#!/usr/bin/env bash

set -e

VBoxManage controlvm minikube savestate
sleep 5