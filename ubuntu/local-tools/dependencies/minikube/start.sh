#!/usr/bin/env bash

set +e

if [[ $(VBoxManage startvm minikube --type headless) ]]; then
    sleep 4
    minikube start
else
    minikube delete
    minikube start
fi