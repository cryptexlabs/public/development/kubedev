#!/usr/bin/env bash

green='\033[0;32m'
yellow='\033[0;93m'
no_color='\033[0m'

echo ""
echo -e "${green}Updating apt-get${no_color}"

set +e

# Stop any existing update processes
sudo killall apt apt-get
sudo rm /var/lib/apt/lists/lock
sudo rm /var/cache/apt/archives/lock
sudo rm /var/lib/dpkg/lock*
sudo dpkg --configure -a

set -e

sudo apt-get -y dist-upgrade
sudo apt-get -y update
sudo apt-get -y upgrade