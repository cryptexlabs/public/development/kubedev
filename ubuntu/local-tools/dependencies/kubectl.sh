#!/usr/bin/env bash

set -e

# Colors
green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ ! $(which kubectl) ]]; then
    echo -e "${green}installing kubectl${no_color}"
    sudo apt-get -y install apt-transport-https
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
    echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
    sudo apt-get -y update
    sudo apt-get -y install kubectl
else
    echo -e "${green}kubectl already installed. Updating kubectl${no_color}"
    sudo apt-get -y install --only-upgrade kubectl
fi

