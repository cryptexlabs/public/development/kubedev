#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

install_chrome() {
    wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
    sudo sh -c 'echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
    sudo apt-get -y update
    sudo apt-get -y install google-chrome-stable
}

echo ""
if [[ ! -d /usr/lib/google-chrome-stable ]]; then
    echo -e "${green}installing google chrome${no_color}"
    install_chrome
else
    echo -e "${green}google chrome already installed${no_color}"
    sudo apt-get -y install --only-upgrade google-chrome-stable
fi

echo "Adding util for adding self signed certificates"
sudo apt-get install libnss3-tools
mkdir -p $HOME/.pki/nssdb

echo "Making chrome your default browser"
sudo update-alternatives --install /usr/bin/x-www-browser x-www-browser /usr/bin/google-chrome-stable 200
sudo update-alternatives --set x-www-browser /usr/bin/google-chrome-stable