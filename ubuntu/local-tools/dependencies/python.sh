#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ -z $(brew ls | grep python3) ]]; then
    echo -e "${green}installing python 3${no_color}"
    apt-get -y install python3
else
    echo -e "${green}python 3 is already installed. Updating python 3${no_color}"
    set +e
    sudo apt-get -y install --only-upgrade python3
    set -e
fi