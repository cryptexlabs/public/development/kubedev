#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ ! $(which vagrant) ]]; then
    echo -e "${green}installing vagrant${no_color}"
    sudo apt-get -y install vagrant
else
    echo -e "${green}vagrant already installed. Updating vagrant${no_color}"
    sudo apt-get -y install --only-upgrade vagrant
fi