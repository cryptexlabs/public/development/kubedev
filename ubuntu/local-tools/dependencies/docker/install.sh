#!/usr/bin/env bash

set -e

green='\033[0;32m'
yellow='\033[0;93m'
no_color='\033[0m'

install_docker() {
    sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    sudo apt-get -y update
    sudo apt-get -y install docker-ce
    mkdir -p ~/.docker/
    echo "{}" > ~/.docker/config.json
    sudo gpasswd -a $(whoami) docker
    sudo systemctl restart docker
}

echo ""
if [[ ! $(which docker) ]]; then
    echo -e "${green}installing docker${no_color}"
    install_docker
else
    echo -e "${green}docker already installed. Updating docker${no_color}"
    sudo apt-get -y install --only-upgrade docker-ce
fi