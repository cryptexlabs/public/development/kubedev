#!/usr/bin/env bash

set -e
sudo systemctl restart docker
echo "🐳 🐳 🐳 🐳 🐳 🐳 🐳 Launching Docker for Ubuntu 🐳 🐳 🐳 🐳 🐳 🐳 🐳"

count=0
set +e
while ! $(docker ps 1> /dev/null 2> /dev/null); do
    echo -n "."
    sleep 1
    ((count++))
    if (( ${count} >= 4 )); then
        echo -en "\r\033[K"
        count=0
    fi
done
set -e
echo ""
echo "⛵ ⛵ ⛵ ⛵ ⛵ ⛵ ⛵  Docker for Ubuntu Launched! ⛵ ⛵ ⛵ ⛵ ⛵ ⛵ ⛵"
echo "🌊 🌊 🌊 🌊 🌊 🌊 🌊        Happy Sailing!        🌊 🌊 🌊 🌊 🌊 🌊 🌊"

if [[ -d /usr/lib/open-vm-tools ]] || [[ -d /usr/lib/open-vm-tools-desktop ]]; then
    echo "Stopping Docker in VMware to save memory space"
    echo "This should only happen when running this utility on an operating system with vmware tools installed"
    sudo systemctl stop docker
fi