#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))

echo ""
if [[ ! $(which virtualbox) ]]; then
    echo -e "${green}installing virtualbox${no_color}"
    wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
    wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
    sudo add-apt-repository "deb http://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib"
    sudo apt-get -y update
    sudo apt-get -y install virtualbox-6.0
else
    echo -e "${green}virtualbox already installed. Updating virtualbox${no_color}"
    sudo apt-get -y install --only-upgrade virtualbox
fi

# Add current user to vboxusers:
sudo gpasswd -a $(whoami) vboxusers

# Setup virtualbox initializer service
sudo cp ${SCRIPT_DIRECTORY}/virtualbox-initializer.service /etc/systemd/system/virtualbox-initializer.service
sudo mkdir -p /usr/lib/vbox/bin
sudo cp ${SCRIPT_DIRECTORY}/vbox-initialize.sh /usr/lib/vbox/bin/vbox-initialize
sudo chmod +x /usr/lib/vbox/bin/vbox-initialize

sudo systemctl daemon-reload
sudo systemctl enable virtualbox-initializer