#!/usr/bin/env bash

set -e

/sbin/vboxconfig

if [[ -z $(VBoxManage list hostonlyifs | grep vboxnet0) ]]; then
    VBoxManage hostonlyif create
fi