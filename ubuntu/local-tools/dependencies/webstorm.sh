#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ -z $(sudo snap list | grep webstorm) ]]; then
    echo -e "${green}installing webstorm${no_color}"
    sudo snap install webstorm --classic
else
    echo -e "${green}webstorm already installed. Updating webstorm${no_color}"
    sudo snap refresh webstorm --classic
fi