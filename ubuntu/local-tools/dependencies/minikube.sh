#!/usr/bin/env bash

set -e

green='\033[0;32m'
yellow='\033[0;93m'
no_color='\033[0m'

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))
UBUNTU_DIRECTORY=$(dirname $(dirname ${SCRIPT_DIRECTORY}))
PROJECT_DIRECTORY=$(dirname ${UBUNTU_DIRECTORY})

# Helper functions
source "${PROJECT_DIRECTORY}/util/bash/fn.sh"

minikube_latest=$(get_github_latest_release "kubernetes/minikube")
install_minikube() {
    # MiniKube
    curl -Lo ~/Downloads/minikube https://storage.googleapis.com/minikube/releases/${minikube_latest}/minikube-linux-amd64
    chmod +x ~/Downloads/minikube
    sudo cp -f ~/Downloads/minikube /usr/local/bin/minikube
    rm ~/Downloads/minikube
}

echo ""
if [[ ! $(which minikube) ]]; then
    echo -e "${green}installing minikube${no_color}"
    install_minikube
else
    echo -e "${green}minikube already installed${no_color}"
    current_version_output=$(minikube version)
    current_version=${current_version_output//minikube version: /}
    if [[ "${current_version}" != "${minikube_latest}" ]]; then
        echo -e "${yellow}current minikube version is out of date${no_color}"
        echo -e "${green}updating minikube${no_color}"
        install_minikube
    fi
fi

# Launch minikube
minikube start

# Auto start minikube in headless mode on startup
sudo systemctl enable vboxvmservice@minikube.service

# Enable ingress controllers
minikube addons enable ingress

# Enable metrics server
minikube addons enable metrics-server