#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
echo -e "${green}installing minikube-dns${no_color}"

# Install dns server
rm -rf /tmp/minikube-dns
git clone git@gitlab.com:cryptexlabs/public/development/minikube-dns.git /tmp/minikube-dns
/tmp/minikube-dns/k8s/bin/install.sh

# Update dns servers on host
mkdir -p /etc/resolvconf/resolv.conf.d/
touch /etc/resolvconf/resolv.conf.d/head
echo "nameserver $(minikube ip)" >> /etc/resolvconf/resolv.conf.d/head
sudo service resolvconf restart

rm -rf /tmp/minikube-dns