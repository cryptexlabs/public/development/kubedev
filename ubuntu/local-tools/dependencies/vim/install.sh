#!/usr/bin/env bash

set -e

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))

cp ${SCRIPT_DIRECTORY}/.vimrc ~/.vimrc
sudo cp ${SCRIPT_DIRECTORY}/.vimrc /root/.vimrc