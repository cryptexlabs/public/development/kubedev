#!/usr/bin/env bash

set -e

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Colors
red='\033[0;31m'
no_color='\033[0m'

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))
UBUNTU_DIRECTORY=$(dirname ${SCRIPT_DIRECTORY})c
PROJECT_DIRECTORY=$(dirname ${UBUNTU_DIRECTORY})

# Helper functions
source "${PROJECT_DIRECTORY}/util/bash/fn.sh"

# Tools required before setting up ssh
${SCRIPT_DIRECTORY}/dependencies/apt.sh
${SCRIPT_DIRECTORY}/dependencies/curl.sh
${SCRIPT_DIRECTORY}/dependencies/git.sh