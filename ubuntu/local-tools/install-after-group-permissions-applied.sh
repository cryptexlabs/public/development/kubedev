#!/usr/bin/env bash

set -e

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Colors
green='\033[0;32m'
no_color='\033[0m'

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))

${SCRIPT_DIRECTORY}/dependencies/docker/wait-for-launch.sh
${SCRIPT_DIRECTORY}/dependencies/java.sh
${SCRIPT_DIRECTORY}/dependencies/kubectl.sh
${SCRIPT_DIRECTORY}/dependencies/kubectx.sh
${SCRIPT_DIRECTORY}/dependencies/minikube/install.sh
${SCRIPT_DIRECTORY}/dependencies/hosts-manager.sh
${SCRIPT_DIRECTORY}/dependencies/helm.sh
${SCRIPT_DIRECTORY}/dependencies/skaffold.sh
${SCRIPT_DIRECTORY}/dependencies/yarn.sh
${SCRIPT_DIRECTORY}/dependencies/gpg.sh
${SCRIPT_DIRECTORY}/dependencies/webstorm.sh
${SCRIPT_DIRECTORY}/dependencies/brave.sh

# Disable as currently does not work on mac due to changes in dns system (most likely a security update)
#${SCRIPT_DIRECTORY}/dependencies/minikube-dns.sh