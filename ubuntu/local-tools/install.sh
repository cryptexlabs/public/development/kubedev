#!/usr/bin/env bash

set -e

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Colors
green='\033[0;32m'
no_color='\033[0m'

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))

#mkdir -p /usr/local/lib/pkgconfig

# Local development environment dependencies
${SCRIPT_DIRECTORY}/dependencies/vim/install.sh
${SCRIPT_DIRECTORY}/dependencies/virtualbox/install.sh
${SCRIPT_DIRECTORY}/dependencies/docker/install.sh

# Install things after group permissions have been applied
sudo su - $(whoami) -l -c "${SCRIPT_DIRECTORY}/install-after-group-permissions-applied.sh"