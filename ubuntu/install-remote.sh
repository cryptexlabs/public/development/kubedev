#!/usr/bin/env bash

set -e

# Colors
cyan='\033[0;36m'
no_color='\033[0m'

# Download the dev kubedev repository
if [[ $(sudo -n uptime 2>&1|grep "load"|wc -l) -le 0 ]]; then
    echo -e "${cyan}Please enter your password to begin installation:${no_color}"
fi

sudo mkdir -p /usr/local/kubedev/tmp
sudo rm -rf /usr/local/kubedev/tmp/*
sudo wget -O /usr/local/kubedev/tmp/kubedev.zip https://gitlab.com/api/v4/projects/12049042/repository/archive.zip
sudo unzip -qq /usr/local/kubedev/tmp/kubedev.zip -d /usr/local/kubedev/tmp/out
sudo rm -rf /usr/local/kubedev/dist
sudo find /usr/local/kubedev/tmp/out -name "kubedev-master-*" -type d -exec cp -Rf {} /usr/local/kubedev/dist \;
sudo find /usr/local/kubedev/dist -type f -iname "*.sh" -exec chmod +x {} \;
sudo rm -rf /usr/local/kubedev/tmp

/usr/local/kubedev/dist/ubuntu/install.sh $1