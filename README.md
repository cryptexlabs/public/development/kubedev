# Kubedev

This will install all the things a developer needs to get started with local application development with Kubernetes. The things that are installed are done so at the opinion of the authors as being necessary tools for successful application developments on kubernetes in your local development environment.

## System requirements

#### Memory
4.5 GB of memory must be available to successfully complete the installation

#### Virtualization
If you are running this in a virtual machine like vmware you must enable hypervisor virtualization in your virtual machine settings. 
This allows a the Docker virtual machine and virtualbox to run inside your virtual machine.

## Mac OS

To install kubedev Mac OS please run the following command:

```bash
bash <(curl -s https://gitlab.com/cryptexlabs/public/development/kubedev/raw/master/macos/install-remote.sh)
```

## Ubuntu

To install kubedev on Ubuntu please run the following command:

```bash
bash <(wget -o /dev/null -O - https://gitlab.com/cryptexlabs/public/development/kubedev/raw/master/ubuntu/install-remote.sh)
```