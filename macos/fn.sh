#!/usr/bin/env bash

git_is_installed() {
    xpath=$(xcode-select --print-path 2>/dev/null)
    if [[ -d "${xpath}" ]]; then
        return 0
    else
        return 1
    fi
}