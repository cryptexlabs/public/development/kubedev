#!/usr/bin/env bash

set -e

green='\033[0;32m'
cyan='\033[0;36m'
no_color='\033[0m'

echo ""
if [[ ! -f /etc/sudoers.bk ]]; then
    echo -e "${green}Enabling passwordless sudo for installation${no_color}"
    if [[ $(sudo -n uptime 2>&1|grep "load"|wc -l) -le 0 ]]; then
        echo -e "${cyan}Please enter your password:${no_color}"
    fi

    sudo cp /etc/sudoers /etc/sudoers.bk
    sudo chown root /etc/sudoers.bk

    sudo sed -i '' 's/root ALL=(ALL) ALL/root ALL=(ALL) NOPASSWD: ALL/g' /etc/sudoers
    sudo sed -i '' 's/root		ALL = (ALL) ALL/root		ALL = (ALL) NOPASSWD: ALL/g' /etc/sudoers
    sudo sed -i '' 's/$(whoami)		ALL = (ALL) ALL/$(whoami)		ALL = (ALL) NOPASSWD: ALL/g' /etc/sudoers
    sudo sed -i '' "s/$(whoami) ALL=(ALL) ALL/$(whoami) ALL=(ALL) NOPASSWD: ALL/g" /etc/sudoers
fi