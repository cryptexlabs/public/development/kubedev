#!/usr/bin/env bash

set -e

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))
PROJECT_DIRECTORY=$(dirname ${SCRIPT_DIRECTORY})
OPERATING_SYSTEM=$(basename ${SCRIPT_DIRECTORY})

${PROJECT_DIRECTORY}/util/bash/install.sh "${OPERATING_SYSTEM}" $1