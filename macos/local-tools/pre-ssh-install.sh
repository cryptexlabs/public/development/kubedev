#!/usr/bin/env bash

set -e

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Colors
red='\033[0;31m'
no_color='\033[0m'

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))
MACOS_DIRECTORY=$(dirname ${SCRIPT_DIRECTORY})
PROJECT_DIRECTORY=$(dirname ${MACOS_DIRECTORY})

# Helper functions
source "${PROJECT_DIRECTORY}/util/bash/fn.sh"

${SCRIPT_DIRECTORY}/permissions.sh

# Tools required before setting up ssh
set +e
while timeout $((60 * 5)) "${SCRIPT_DIRECTORY}/dependencies/homebrew.sh"; [[ $? -ne 0 ]]; do
    echo -e "${red}Brew install failed. Retrying...${no_color}"
done
set -e

# Bash 5+ (depends on brew)
${SCRIPT_DIRECTORY}/dependencies/bash.sh