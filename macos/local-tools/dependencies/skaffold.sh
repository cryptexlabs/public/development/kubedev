#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ ! $(which skaffold) ]]; then
    echo -e "${green}installing skaffold${no_color}"
    brew install skaffold
else
    echo -e "${green}skaffold already installed. Updating skaffold${no_color}"
    set +e
    brew upgrade skaffold 2> /dev/null
    set -e
fi