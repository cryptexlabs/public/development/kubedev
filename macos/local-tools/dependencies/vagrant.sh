#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ ! $(which vagrant) ]]; then
    echo -e "${green}installing vagrant${no_color}"
    brew cask install vagrant
else
    echo -e "${green}vagrant already installed. Updating vagrant${no_color}"
    set +e
    brew cask upgrade vagrant 2> /dev/null
    set -e
fi