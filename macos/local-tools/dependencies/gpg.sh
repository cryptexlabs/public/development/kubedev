#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ ! $(which gpg) ]]; then
    echo -e "${green}installing gpg${no_color}"
    brew install gpg
else
    echo -e "${green}gpg already installed. Updating gpg${no_color}"
    set +e
    brew upgrade gpg 2> /dev/null
    set -e
fi