#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ $(which bash) = "/bin/bash" ]]; then
    echo -e "${green}installing latest version of bash with homebrew${no_color}"
    brew install bash
    sudo cat /etc/shells > /tmp/shells
    sudo echo "/usr/local/bin/bash" >> /tmp/shells
    sudo chown root:wheel /tmp/shells
    sudo mv /tmp/shells /etc/shells
    sudo chsh -s /usr/local/bin/bash
else
    echo -e "${green}bash already installed. Updating bash${no_color}"
    set +e
    brew upgrade bash 2> /dev/null
    set -e
fi