#!/usr/bin/env bash

set -e

realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))

green='\033[0;32m'
yellow='\033[0;93m'
red='\033[0;31m'
no_color='\033[0m'

if [[ ! $(which virtualbox) ]]; then

    # To enable virtual box you can also start mac os in safe mode run this command in a terminal
    # `spctl kext-consent add VB5E2TV963`

    install_succeeded=false
    while [[ ${install_succeeded} != "true" ]]; do

        echo ""
        echo -e "${green}installing virtualbox${no_color}"

        set +e
        brew cask install virtualbox

        if [[ $? -ne 0 ]]; then

            rm -rf /usr/local/bin/virtualbox
            brew cask uninstall virtualbox 2> /dev/null

            echo -e "${red}The virtual box installation failed"
            echo -e "Please enable the system extension by following the prompt for 'System Extension Blocked'"
            echo -e "Click on the 'Open Security Preferences' button to navigate to system preferences page"
            echo ""
            echo -e "${yellow}Navigate to System Preferences -> Security & Privacy -> General"
            echo -e "Then click the 'Allow' button for the developer 'Oracle America, Inc'"
            echo -e "This script will automatically retry installation in 20 seconds.${no_color}"

            sleep 20
        else
            install_succeeded="true"
        fi

        set -e
    done

    sudo spctl --add /Applications/VirtualBox.app

    sudo cp ${SCRIPT_DIRECTORY}/vbox.plist /Library/LaunchDaemons/org.virtualbox.vboxautostart.plist
    sudo chown root:wheel /Library/LaunchDaemons/org.virtualbox.vboxautostart.plist
    sed "s/{{user}}/$(whoami)/g" ${SCRIPT_DIRECTORY}/autostart.cfg > /tmp/vbox-autostart.cfg
    sudo mkdir -p /etc/vbox/
    sudo mv /tmp/vbox-autostart.cfg /etc/vbox/autostart.cfg
    sudo launchctl load /Library/LaunchDaemons/org.virtualbox.vboxautostart.plist
else
    echo ""
    echo -e "${green}virtualbox already installed. Updating virtualbox${no_color}"
    brew cask upgrade virtualbox 2> /dev/null
fi