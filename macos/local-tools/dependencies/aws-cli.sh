#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ -z $(which aws) ]]; then
    echo -e "${green}installing aws cli${no_color}"
    pip3 install awscli --upgrade --user
else
    echo -e "${green}aws cli is already installed. Updating aws cli${no_color}"
    pip3 install awscli --upgrade --user
fi