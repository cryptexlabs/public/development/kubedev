#!/usr/bin/env bash

set -e

green='\033[0;32m'
yellow='\033[0;93m'
no_color='\033[0m'

echo ""
set +e
xpath=$(xcode-select --print-path 2>/dev/null)
set -e
if [[ ! $(which brew) ]] || [[ ! -d "${xpath}" ]]; then
    echo -e "${green}installing homebrew${no_color}"
    echo -e "\n" | /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
else
    echo -e "${green}homebrew already installed. Updating homebrew${no_color}"
    brew update
fi

mkdir -p /usr/local/lib/pkgconfig
sudo chown -R $(whoami) /usr/local/lib/pkgconfig
chmod u+w /usr/local/lib/pkgconfig