#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ ! -d "/Applications/Brave Browser.app/" ]]; then
    echo -e "${green}installing brave browser${no_color}"
    brew cask install brave-browser
    sudo spctl --add "/Applications/Brave Browser.app/"
else
    echo -e "${green}brave browser already installed${no_color}"

    if [[ ! -z $(brew ls --versions brave-browser) ]]; then
        set +e
        brew cask upgrade brave-browser 2> /dev/null
        set -e
    fi
fi

echo "Making brave your default browser"
echo "Please follow the UI Prompts"
sudo open -a "Brave Browser" --args --make-default-browser &


echo "Brave is set as your default browser"