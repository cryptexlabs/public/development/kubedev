#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

mkdir -p /usr/local/lib/pkgconfig
sudo chown -R $(whoami) /usr/local/lib/pkgconfig
chmod u+w /usr/local/lib/pkgconfig

echo ""
if [[ ! $(which helm) ]]; then
    echo -e "${green}installing helm${no_color}"
    brew install kubernetes-helm
    helm init
else
    echo -e "${green}helm already installed. Updating helm${no_color}"
    set +e
    brew upgrade kubernetes-helm 2> /dev/null
    set -e
fi

if [[ -z $(helm plugin list | grep secrets) ]]; then
    echo "Installing Helm 'secrets' plugin"
    helm plugin install https://github.com/futuresimple/helm-secrets
else
    echo "Helm 'secrets' plugin already installed. Updating plugin"
    helm plugin update secrets
fi

if [[ -z $(helm plugin list | grep delete-all) ]]; then
    echo "Installing helm 'delete-all' plugin"
    helm plugin install https://github.com/astronomerio/helm-delete-all-plugin --version 0.0.2
else
    echo "Helm 'delete-all' plugin already installed. Updating plugin"
    helm plugin update delete-all
fi