#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ -z $(brew ls | grep aws-iam-authenticator) ]]; then
    echo -e "${green}installing aws iam authenticator${no_color}"
    brew install aws-iam-authenticator
else
    echo -e "${green}aws iam authenticator is already installed. Updating aws iam authenticator${no_color}"
    set +e
    brew upgrade aws-iam-authenticator 2> /dev/null
    set -e
fi