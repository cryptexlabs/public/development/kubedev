#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

mkdir -p /usr/local/lib/pkgconfig
sudo chown -R $(whoami) /usr/local/lib/pkgconfig
chmod u+w /usr/local/lib/pkgconfig

echo ""
if [[ ! $(which yarn) ]]; then
    echo -e "${green}installing yarn${no_color}"
    brew install yarn
else
    echo -e "${green}yarn already installed. Updating yarn${no_color}"
    set +e
    brew upgrade yarn 2> /dev/null
    set -e
fi