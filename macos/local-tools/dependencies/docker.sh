#!/usr/bin/env bash

set -e

green='\033[0;32m'
yellow='\033[0;93m'
no_color='\033[0m'

install_docker() {
    brew cask install docker

    # Remove kubectl so we can install it with brew
    rm -f $(which kubectl)

    echo "🐳 🐳 🐳 🐳 🐳 🐳 🐳 Launching Docker for Mac 🐳 🐳 🐳 🐳 🐳 🐳 🐳"
    echo "Please Check your tray for status."
    echo -e "${yellow}Please follow the UI prompts and enter your admin password${no_color}"
    sudo spctl --add /Applications/Docker.app
    sudo xattr -d -r com.apple.quarantine /Applications/Docker.app
    open /Applications/Docker.app

    count=0
    set +e
    while ! $(docker ps 1> /dev/null 2> /dev/null); do
        echo -n "."
        sleep 1
        ((count++))
        if (( ${count} >= 4 )); then
            echo -en "\r\033[K"
            count=0
        fi
    done
    set -e
    echo ""
    echo "⛵ ⛵ ⛵ ⛵ ⛵ ⛵ ⛵ Docker for Mac Launched!  ⛵ ⛵ ⛵ ⛵ ⛵ ⛵ ⛵"
    echo "🌊 🌊 🌊 🌊 🌊 🌊 🌊      Happy Sailing!       🌊 🌊 🌊 🌊 🌊 🌊 🌊"

    if [[ ! -z "$(ls '/Library/Application Support/' | grep 'VMware Tools')" ]]; then
        echo "Stopping Docker in VMware to save memory space"
        echo "This should only happen when running this utility on an operating system with vmware tools installed"
        osascript -e 'quit app "Docker"'
    fi
}

echo ""
if [[ ! $(which docker) ]]; then
    echo -e "${green}installing docker${no_color}"
    install_docker
else
    echo -e "${green}docker already installed${no_color}"
    set +e
    brew cask upgrade docker 2> /dev/null
    set -e
fi