#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ ! $(command -v kubectx) ]]; then
    echo -e "${green}installing kubectx${no_color}"
    # Install nvm
    brew install kubectx
else
    echo -e "${green}kubectx already installed. Updating kubectx${no_color}"
    set +e
    brew upgrade kubectx 2> /dev/null
    set -e
fi