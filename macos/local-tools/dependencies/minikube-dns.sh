#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
echo -e "${green}installing minikube-dns${no_color}"

# Install minikube dns
rm -rf ~/Downloads/minikube-dns
git clone git@gitlab.com:cryptexlabs/public/development/minikube-dns.git ~/Downloads/minikube-dns
~/Downloads/minikube-dns/k8s/bin/install.sh

set +e
networksetup -setdnsservers Wi-Fi $(minikube ip) 8.8.8.8 8.8.4.4
networksetup -setdnsservers Ethernet $(minikube ip) 8.8.8.8 8.8.4.4

rm -rf ~/Downloads/minikube-dns