#!/usr/bin/env bash

set -e

green='\033[0;32m'
yellow='\033[0;93m'
no_color='\033[0m'

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))
MACOS_DIRECTORY=$(dirname $(dirname $(dirname ${SCRIPT_DIRECTORY})))
PROJECT_DIRECTORY=$(dirname ${MACOS_DIRECTORY})

echo ""
if [[ ! $(which minikube) ]]; then
    echo -e "${green}installing minikube${no_color}"
    brew cask install minikube
else
    echo -e "${green}minikube already installed${no_color}"

    if [[ ! -z $(brew cask ls --versions minikube) ]]; then
        set +e
        brew cask upgrade minikube 2> /dev/null
        set -e
    fi
fi

# Launch minikube
if [[ -z $(minikube status | grep Running) ]]; then
    minikube start
fi

sed "s/{{user}}/$(whoami)/g" ${SCRIPT_DIRECTORY}/minikube.plist > /tmp/org.virtualbox.minikubeautostart.plist
sudo mv /tmp/org.virtualbox.minikubeautostart.plist /Library/LaunchDaemons/org.virtualbox.minikubeautostart.plist
sudo chown root:wheel /Library/LaunchDaemons/org.virtualbox.minikubeautostart.plist
sudo launchctl load /Library/LaunchDaemons/org.virtualbox.minikubeautostart.plist

# Enable ingress controllers
minikube addons enable ingress

# Enable metrics server
minikube addons enable metrics-server