#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ ! -d "/Applications/WebStorm.app" ]]; then
    echo -e "${green}installing webstorm${no_color}"
    brew cask install webstorm
else
    echo -e "${green}webstorm already installed${no_color}"

    if [[ ! -z $(brew ls --versions webstorm) ]]; then
        set +e
        brew cask upgrade webstorm 2> /dev/null
        set -e
    fi
fi