#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ ! $(which hosts) ]]; then
    echo -e "${green}installing hosts manager${no_color}"
    brew tap alphabetum/taps
    brew install alphabetum/taps/hosts
else
    echo -e "${green}hosts manager already installed. Updating hosts manager${no_color}"
    set +e
    brew upgrade alphabetum/taps/hosts 2> /dev/null
    set -e
fi