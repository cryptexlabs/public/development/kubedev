#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ ! -d "/Applications/Google Chrome.app" ]]; then
    echo -e "${green}installing google chrome${no_color}"
    brew cask install google-chrome
    sudo spctl --add "/Applications/Google Chrome.app"
else
    echo -e "${green}google chrome already installed${no_color}"

    if [[ ! -z $(brew ls --versions google-chrome) ]]; then
        set +e
        brew cask upgrade google-chrome 2> /dev/null
        set -e
    fi
fi

echo "Making chrome your default browser"
echo "Please follow the UI Prompts"
sudo open -a "Google Chrome" --args --make-default-browser &


echo "Chrome is set as your default browser"