#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ -z $(brew ls | grep python3) ]]; then
    echo -e "${green}installing python 3${no_color}"
    brew install python3
    echo 'export PATH=~/Library/Python/3.7/bin:$PATH' >> ~/.bash_profile
else
    echo -e "${green}python 3 is already installed. Updating python 3${no_color}"
    set +e
    brew upgrade python3 2> /dev/null
    set -e
fi