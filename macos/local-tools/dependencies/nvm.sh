#!/usr/bin/env bash

set -e

green='\033[0;32m'
no_color='\033[0m'

echo ""
if [[ ! $(command -v nvm) ]]; then
    echo -e "${green}installing nvm${no_color}"
    # Install nvm
    brew install nvm
    mkdir ~/.nvm
    {
      echo 'export NVM_DIR="$HOME/.nvm"'
      echo '[ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh"'
      echo '[ -s "/usr/local/opt/nvm/etc/bash_completion" ] && . "/usr/local/opt/nvm/etc/bash_completion"'
    } >> ~/.bash_profile
else
    echo -e "${green}nvm already installed. Updating nvm${no_color}"
    set +e
    brew upgrade nvm 2> /dev/null
    set -e
fi

