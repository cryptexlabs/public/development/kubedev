#!/usr/bin/env bash

set -e

# Colors
green='\033[0;32m'
no_color='\033[0m'

realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

SCRIPT_DIRECTORY=$(dirname $(realpath $0))
PROJECT_DIRECTORY=$(dirname $(dirname ${SCRIPT_DIRECTORY}))

did_prompt_user="false"
source ${PROJECT_DIRECTORY}/util/bash/get-user-info.sh "${PROJECT_DIRECTORY}/macos"

if [[ "${did_prompt_user}" -eq "true" ]] ; then
    echo ""
    echo ""
    echo -e "${green}That's all the info we need from you!${no_color}"
fi

sleep_total=0
if [[ ! $(which virtualbox) ]] || [[ ! $(which docker) ]]; then
    echo -e "${green}Unfortunately there will be a few more interactions we need from you${no_color}"
    sleep_total=$((${sleep_total}+3))
fi
if [[ ! $(which brew) ]] && ([[ ! $(which virtualbox) ]] || [[ ! $(which docker) ]]); then
    echo -e "${green}After brew is installed in about 5-10 minutes you'll need to do the following:${no_color}"
    sleep_total=$((${sleep_total}+3))
fi
if [[ ! $(which virtualbox) ]]; then
    echo -e "${green}   - Give virtualbox permission to install kernel extension${no_color}"
    sleep_total=$((${sleep_total}+3))
fi
if [[ ! $(which docker) ]]; then
    echo -e "${green}   - Give docker administrative privileges${no_color}"
    sleep_total=$((${sleep_total}+3))
fi
if [[ ! -d "/Applications/Google Chrome.app" ]]; then
    echo -e "${green}   - Enable brave as your default browser${no_color}"
    sleep_total=$((${sleep_total}+3))
fi

if [[ ! $(which virtualbox) ]] || [[ ! $(which docker) ]] || [[ ! -d "/Applications/Google Chrome.app" ]]; then
    echo ""
fi


sleep ${sleep_total}