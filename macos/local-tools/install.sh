#!/usr/bin/env bash

set -e

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Colors
green='\033[0;32m'
no_color='\033[0m'

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))

mkdir -p /usr/local/lib/pkgconfig

# Local development environment dependencies
${SCRIPT_DIRECTORY}/dependencies/virtualbox/install.sh
${SCRIPT_DIRECTORY}/dependencies/docker.sh
${SCRIPT_DIRECTORY}/dependencies/brave.sh

echo ""
echo -e "${green}That's it! No more information will be needed from you! Feel free to grab a coffee and relax!${no_color}"
echo ""

sleep 5

${SCRIPT_DIRECTORY}/dependencies/minikube/install.sh
${SCRIPT_DIRECTORY}/dependencies/helm.sh
${SCRIPT_DIRECTORY}/dependencies/kubectx.sh
${SCRIPT_DIRECTORY}/dependencies/yarn.sh
${SCRIPT_DIRECTORY}/dependencies/nvm.sh
${SCRIPT_DIRECTORY}/dependencies/gpg.sh
${SCRIPT_DIRECTORY}/dependencies/python.sh
${SCRIPT_DIRECTORY}/dependencies/aws-cli.sh
${SCRIPT_DIRECTORY}/dependencies/aws-iam-authenticator.sh
${SCRIPT_DIRECTORY}/dependencies/webstorm.sh
${SCRIPT_DIRECTORY}/dependencies/hosts-manager.sh
#${SCRIPT_DIRECTORY}/dependencies/minikube-dns.sh